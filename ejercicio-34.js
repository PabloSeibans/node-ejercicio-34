const clientes = [
    {
        id: 1, 
        nombre: 'Leonor'
    }, 
    {
        id: 2, 
        nombre: 'Jacinto'
    }, 
    {
        id: 3, 
        nombre: 'Waldo'
    }
];
const pagos = [
    {
        id: 1, 
        pago: 1000, 
        moneda: 'Bs'
    }, 
    {
        id: 2,
        pago: '1800',
        moneda: 'Bs'
    }
];





//callback Cliente
const getclientesById = (id, callback) => {
    const cliente = clientes.find(cliente => cliente.id === id)
    setTimeout(() => {
      callback(cliente);
    }, 1500);
  }

  getclientesById(1, (clientes) => {
    console.log("callbak clientes:", clientes);
  })

  //Calback pagos
  const getPagosById = (id, callback) => {
    const pago = pagos.find(pagos => pagos.id === id)
      callback(pago);
    }

  getPagosById(2, (pagos) => {
    console.log("callbak pagos:", pagos);
})





//PROMESA
const id = 1;
const getClients = (id) => {
    return new Promise((resolve, reject)=>{
        const cliente = clientes.find(a => a.id === id);
        if (cliente) {
            resolve(cliente);
        } else {
            reject(`No existe el cliente con el id ${id}`);
        }
    })
}
const getPays = (id) => {
    return new Promise((resolve, reject)=>{
        const pago = pagos.find(p => p.id === id);
        if(pago){
            resolve(pago);
        } else {
            reject(`No existe el pago con el id ${id}`);
        }
    });
};




console.log('PROMESA');
getClients(id)
.then(cliente => console.log(cliente))
.catch(error => {
    console.log(error);
});
getPays(id)
.then(pago =>console.log(pago))
.catch(error => {
    console.log(error);
})
getClients(id)
.then(cliente => {
    getPays(id)
    .then(pago => {
        console.log('El Cliente:', cliente.nombre, 'debe un pago de:',pago.pago,pago.moneda);
    })
    .catch(error =>{
        console.log(error);
    })
})
.catch(error => {
    console.log(error);
})







for (let i = 1; i < clientes.length+1; i++) {
    console.log('PROMESA EN CADENA');
    let id = i;
    let nombre;
    getClients(id)
        .then(cliente=> {
            nombre = cliente.nombre;
            //Siempre colocar un return para poder encadenar un then
            return getPays(id);
        })
        .then(pago => {
            console.log('El Cliente:', nombre, 'debe un pago de:',pago['pago'],pago['moneda']);
        })
        .catch(error => {
            console.log(error);
        })



        console.log('ASYNC Y AWAIT');

    const agetInfoClient = async (id) => {
        try{
            const client = await getClients(id);
            const pay = await getPays(id);
            return `El pago del cliente:${client.nombre}es de ${pay['pago']} ${pay['moneda']}`;
        }
        catch(ex){
            throw ex;
        }
    };

    agetInfoClient(id)
    .then(msg => console.log(msg))
    .catch(err =>console.log(err))
}





